Start the server using 'npm start' or 'node server.js' on the default port 3500
This should run on the same node as the multichain software.
Credentials of multichain are picked up from environment settings.
Store PORT,HOST,USERNAME,PASSWORD,MC_VERSION for multichain in .env file.
The APIs are compatible with Multichain 1.0 and Multichain 2.0, as mentioned in the API documentation.

This project will expose REST APIs to publish data to multichain streams or retrieve data based on ID/key.
routes.js contains available REST API paths.

The overall architecture of the project is shown in the figure below:

![alt text](https://gitlab.com/statwig-public/BlockchainEngine/blob/master/architecture.jpeg)
