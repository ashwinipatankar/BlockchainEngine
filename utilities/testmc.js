const init = require('../common/init');
//Faker library is to generate different types of mock records like name,number etc..
faker = require('faker')

function publishTestData(address, stream_name, count) {
    const multichain = init.getMultichain();
    for (var i = 0; i < count; i++) {
        var key = faker.name.firstName();
        var data = faker.lorem.sentence()

        multichain.publishFrom({
            from: address,
            stream: stream_name,
            key: key,
            data: new Buffer(data).toString("hex")
        }, (err, info) => {
            console.log('transaction hash is', info);
        })
    }
}
