const init = require('../common/init');

exports.fetchStreamKeys = function (req, res) {
    console.log('txId ', req.query);
    const {stream, key} = req.query;
    const multichain = init.getMultichain();

    multichain.listStreamKeys({
        stream
    }, (err, data) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        data.forEach(item => {
            item.key = Buffer.from(item.key, 'hex').toString('utf8')
            console.log('item.data', item.key);
        });
        res.json({items: key});
    });
}
